package be.aca.devcon.jockey.jsf.internal.resolver;

import org.osgi.framework.BundleContext;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceReference;

import javax.el.ELContext;
import javax.el.ELResolver;
import java.beans.FeatureDescriptor;
import java.util.Iterator;

public class OSGiServiceELResolver extends ELResolver {

	public Object getValue(ELContext context, Object base, Object property) {
		if (base == null) {
			String serviceName = property.toString();
			Object service = this.getServiceByName(serviceName);
			if (service != null) {
				context.setPropertyResolved(true);
			}
			return service;
		}
		return null;
	}

	protected Object getServiceByName(String name) {
		try {
			BundleContext bundleContext = FrameworkUtil.getBundle(getClass()).getBundleContext();

			String simpleClassName = name.substring(0, 1).toUpperCase() + name.substring(1);
			ServiceReference[] serviceReferences = bundleContext.getAllServiceReferences(null, "(objectClass=*." + simpleClassName + ")");

			if (serviceReferences != null && serviceReferences.length > 0) {
				return bundleContext.getService(serviceReferences[0]);
			}
		} catch (InvalidSyntaxException e) {
			return null;
		}
		return null;
	}

	public Class<?> getType(ELContext context, Object base, Object property) {
		return null;
	}

	public void setValue(ELContext context, Object base, Object property, Object value) {
	}

	public boolean isReadOnly(ELContext context, Object base, Object property) {
		return true;
	}

	public Iterator<FeatureDescriptor> getFeatureDescriptors(ELContext context, Object base) {
		return null;
	}

	public Class<?> getCommonPropertyType(ELContext context, Object base) {
		return Object.class;
	}
}
